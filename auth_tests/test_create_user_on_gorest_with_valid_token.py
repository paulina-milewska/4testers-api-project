from faker import Faker
from random import choice
import requests


def test_user_create_pass_with_valid_token():
    url = "https://gorest.co.in/public/v2/users"
    headers = {
        "Authorization": "Bearer 1ec920474721f255b266c7319db908f253c7f22c51743eb7cd8279749e4f46ae"
    }
    user_data = {
        "name": Faker().name(),
        "gender": choice(["male", "female"]),
        "email": Faker().email(),
        "status": "active"
    }
    response_post = requests.post(url, headers=headers, data=user_data)
    assert response_post.status_code == 201
    assert response_post.reason == "Created"
    user_id = response_post.json()['id']
    print(user_id)
    user_url = url + "/" + str(user_id)
    response_get = requests.get(user_url, headers=headers)
    assert response_get.status_code == 200
    assert response_get.reason == "OK"
    received_user = response_get.json()
    assert received_user["name"] == user_data["name"]
    assert received_user["gender"] == user_data["gender"]
    assert received_user["email"] == user_data["email"]
    assert received_user["status"] == user_data["status"]